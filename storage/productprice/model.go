package productprice

import (
	"time"

	"gitlab.com/jasforfan/price-monitor/api/config"
	"github.com/satori/go.uuid"
)

type ProductPrice struct {
	Id 				string 		`json:"id,omitempty"`
	ProductId		string		`json:"product_id,omitempty"`
	Price			float64 	`json:"price,omitempty"`
	Currency 		string 		`json:"currency",omitempty"`
	Created			time.Time	`json:"created",omitempty"`
}

type ProductPriceStorage struct {
	writer		Writer
	reader		Reader
}

func New(c *config.Config) *ProductPriceStorage {
	writer := NewMysqlWriter(c.MysqlHost, c.MysqlSchema, c.MysqlUser, c.MysqlPassword)
	reader := NewMysqlReader(c.MysqlHost, c.MysqlSchema, c.MysqlUser, c.MysqlPassword)

	return &ProductPriceStorage{
		writer:	writer,
		reader: reader,
	}
}

func (ps *ProductPriceStorage) Add(pp *ProductPrice) (err error) {
	pp.Id = uuid.Must(uuid.NewV4()).String()

	err = ps.writer.Add(pp)
	return
}

func (ps *ProductPriceStorage) GetByProductId(productId string) (productPrices []*ProductPrice, err error) {
	return ps.reader.GetByProductId(productId)
}