package productprice

type Writer interface {
	Add(productPrice *ProductPrice) (err error)
}

type Reader interface {
	GetByProductId(productId string) (productPrices []*ProductPrice, err error)
}