package productprice

import (
	"bytes"
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

const (
	queryByProductId = "SELECT id, product_id, price, currency, created FROM product_price WHERE product_id = ?"
)

type MysqlWriter struct {
	db 		*sql.DB
}

type MysqlReader struct {
	db 		*sql.DB
}

func NewMysqlWriter(url string, schema string, user string, password string) *MysqlWriter {
	db := connectMysql(url, schema, user, password)
	return &MysqlWriter{db: db}
}

func NewMysqlReader(url string, schema string, user string, password string) *MysqlReader {
	db := connectMysql(url, schema, user, password)
	return &MysqlReader{db: db}
}

func (w *MysqlWriter) Add(pp *ProductPrice) (err error) {
	tx, err := w.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	stmt, err := tx.Prepare("INSERT INTO product_price (id, product_id, price, currency, created) VALUES (?, ?, ?, ?, NOW())")
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(pp.Id, pp.ProductId, pp.Price, pp.Currency)
	
	return
}

func (r *MysqlReader) GetByProductId(productId string) (productPrices []*ProductPrice, err error) {
	rows, err := r.db.Query(queryByProductId + " ORDER BY created", productId)
	if err != nil {
		return
	}

	for rows.Next() {
		var pp ProductPrice
		if err = rows.Scan(&pp.Id, &pp.ProductId, &pp.Price, &pp.Currency, &pp.Created); err != nil {
			return
		}

		productPrices = append(productPrices, &pp)
	}

	return
}

func connectMysql(url string, schema string, user string, password string) *sql.DB {
	var buffer bytes.Buffer
	buffer.WriteString(user)
	buffer.WriteString(":")
	buffer.WriteString(password)
	buffer.WriteString("@tcp(")
	buffer.WriteString(url)
	buffer.WriteString(")/")
	buffer.WriteString(schema)
	buffer.WriteString("?parseTime=true")

	db, err := sql.Open("mysql", buffer.String())
	if err != nil {
		log.Fatalf("Cannot connect to database: %v", err)
	}

	return db
}