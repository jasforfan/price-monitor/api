package product

import (
	"errors"

	"gitlab.com/jasforfan/price-monitor/api/config"
	"github.com/satori/go.uuid"
)

type Writer interface {
	Add(product *Product) (err error)
	Update(product *Product) (err error)
}

type Reader interface {
	GetById(id string) (product *Product, err error)
	GetAll() (products []*Product, err error)
	LinkExists(link string) bool
}

type ProductStorage struct {
	writer		Writer
	reader		Reader
}

func New(c *config.Config) *ProductStorage {
	writer := NewMysqlWriter(c.MysqlHost, c.MysqlSchema, c.MysqlUser, c.MysqlPassword)
	reader := NewMysqlReader(c.MysqlHost, c.MysqlSchema, c.MysqlUser, c.MysqlPassword)

	return &ProductStorage{
		writer:	writer,
		reader: reader,
	}
}

func (ps *ProductStorage) Add(product *Product) (err error) {
	if ps.reader.LinkExists(product.Link) {
		return errors.New("Product exists: " + product.Link)
	}
	product.Id = uuid.Must(uuid.NewV4()).String()
	
	err = ps.writer.Add(product)

	return
}

func (ps *ProductStorage) Update(product *Product) (err error) {
	err = ps.writer.Update(product)
	return
}

func (ps *ProductStorage) GetById(id string) (product *Product, err error) {
	return ps.reader.GetById(id)
}

func (ps *ProductStorage) GetAll() (products []*Product, err error) {
	return ps.reader.GetAll()
}