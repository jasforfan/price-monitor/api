package product

import (
	"bytes"
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

const (
	query = "SELECT id, name, brand, link, price, price_currency, image FROM products"
	queryCount = "SELECT COUNT(1) FROM products"
)

type MysqlWriter struct {
	db 		*sql.DB
}

type MysqlReader struct {
	db 		*sql.DB
}

func NewMysqlWriter(url string, schema string, user string, password string) *MysqlWriter {
	db := connectMysql(url, schema, user, password)
	return &MysqlWriter{db: db}
}

func NewMysqlReader(url string, schema string, user string, password string) *MysqlReader {
	db := connectMysql(url, schema, user, password)
	return &MysqlReader{db: db}
}

func (w *MysqlWriter) Add(p *Product) (err error) {
	tx, err := w.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	stmt, err := tx.Prepare("INSERT INTO products (id, name, brand, link, price, price_currency, image, created) VALUES (?, ?, ?, ?, ?, ?, ?, NOW())")
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(p.Id, p.Name, p.Brand, p.Link, p.Price, p.PriceCurrency, p.Image)
	
	return
}

func (w *MysqlWriter) Update(p *Product) (err error) {
	tx, err := w.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	stmt, err := tx.Prepare("UPDATE products SET name=?, brand=?, link=?, price=?, price_currency=?, image=? WHERE id=?")
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(p.Name, p.Brand, p.Link, p.Price, p.PriceCurrency, p.Image, p.Id)

	return
}

func (r *MysqlReader) GetById(id string) (product *Product, err error) {
	var p Product
	row := r.db.QueryRow(query + " WHERE id = ?", id)
	err = row.Scan(&p.Id, &p.Name, &p.Brand, &p.Link, &p.Price, &p.PriceCurrency, &p.Image)
	return &p, err
}

func (r *MysqlReader) GetAll() (products []*Product, err error) {
	rows, err := r.db.Query(query + " ORDER BY created DESC")
	if err != nil {
		return
	}

	for rows.Next() {
		var p Product
		if err = rows.Scan(&p.Id, &p.Name, &p.Brand, &p.Link, &p.Price, &p.PriceCurrency, &p.Image); err != nil {
			return
		}

		products = append(products, &p)
	}

	return
}

func (r *MysqlReader) LinkExists(link string) bool {
	var count int
	r.db.QueryRow(queryCount + " WHERE link = ?", link).
			Scan(&count)

	return count > 0
}

func connectMysql(url string, schema string, user string, password string) *sql.DB {
	var buffer bytes.Buffer
	buffer.WriteString(user)
	buffer.WriteString(":")
	buffer.WriteString(password)
	buffer.WriteString("@tcp(")
	buffer.WriteString(url)
	buffer.WriteString(")/")
	buffer.WriteString(schema)
	buffer.WriteString("?parseTime=true")

	db, err := sql.Open("mysql", buffer.String())
	if err != nil {
		log.Fatalf("Cannot connect to database: %v", err)
	}

	return db
}
