package product

type Product struct {
	Id				string	`json:"id,omitempty"`
	Name			string	`json:"name,omitempty"`
	Brand			string	`json:"brand,omitempty"`
	Link 			string 	`json:"link,omitempty"`
	Price			float64 `json:"price,omitempty"`
	PriceCurrency	string	`json:"price_currency,omitempty"` 
	Image			string	`json:"image,omitempty"`
}