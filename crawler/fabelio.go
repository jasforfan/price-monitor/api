package crawler

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	st "gitlab.com/jasforfan/price-monitor/api/storage/product"
	"github.com/gocolly/colly"
)

type FabelioCrawler struct {
	url		string
}

func NewFabelioCrawler(url string) *FabelioCrawler {
	return &FabelioCrawler {url: url}
}

func (fc *FabelioCrawler) GetProduct(product *st.Product) (err error) {
	var done bool
	c := colly.NewCollector(
			colly.UserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"),
		)

	c.OnHTML("script[class=y-rich-snippet-script]", func(e *colly.HTMLElement) {
		jsonData := e.Text[strings.Index(e.Text, "{") : len(e.Text)-1]
		
		re := regexp.MustCompile("<[^>]*>[\n]*")
		jsonData = re.ReplaceAllLiteralString(jsonData, "")

		data := struct {
			Context		string 	`json:"@context"`
			Type_ 		string 	`json:"@type"`
			Image		string 	`json:"image"`
			Name		string 	`json:"name"`
			Brand		string 	`json:"brand"`
			Url 		string 	`json:"url"`
			Sku			string 	`json:"sku"`
			Width		string 	`json:"width"`
			Height		string 	`json:"height"`
			Weight		string 	`json:"weight"`
			Offers 		struct {
				Type_ 			string 	`json:"@type"`
				Availability	string 	`json:"availability"`
				Price 			string 	`json:"price"`
				PriceCurrency	string 	`json:"priceCurrency"`
			} `json:"offers"`
		}{}

		err := json.Unmarshal([]byte(jsonData), &data)
		if err != nil {
			done = true
		}

		price, err := strconv.ParseFloat(data.Offers.Price, 32)
		if err != nil {
			done = true
		}

		product.Name = data.Name
		product.Brand = data.Brand
		product.Price = price
		product.PriceCurrency = data.Offers.PriceCurrency
		product.Image = data.Image
		product.Link = data.Url
		done = true
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fc.url)

	for {
		if done {
			break
		}
	}

	return err
}