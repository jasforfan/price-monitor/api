package crawler

import (
	"fmt"
	"net/url"
	st "gitlab.com/jasforfan/price-monitor/api/storage/product"
)

type Crawler interface {
	GetProduct(product *st.Product) error
}

func New(link string) (crawler Crawler, err error) {
	u, err := url.Parse(link)
	if err != nil {
		return
	}
	
	switch u.Hostname() {
	case "fabelio.com":
		fmt.Println(u.Hostname())
		return NewFabelioCrawler(link), nil
	default:
		return
	}

	return
}