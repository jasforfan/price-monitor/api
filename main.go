package main
	
import (
	"fmt"
	"log"
	"github.com/gorilla/mux"
	"net/http"

	svc "gitlab.com/jasforfan/price-monitor/api/service"
	cfg "gitlab.com/jasforfan/price-monitor/api/config"
)

const port = "8000"

func main() {
	conf := cfg.New()
	productService := svc.NewProductService(conf)

	go productService.ReloadScheduler()

	router := mux.NewRouter()
	router.HandleFunc("/products", productService.AddProductLink).Methods("POST")
	router.HandleFunc("/products", productService.GetProducts).Methods("GET")
	router.HandleFunc("/products/{id}", productService.GetProduct).Methods("GET")
	router.HandleFunc("/products/{id}/price", productService.GetProductPrice).Methods("GET")
	fmt.Println("Listening to port ", port)
	log.Fatal(http.ListenAndServe(":"+conf.ServicePort, router))
}