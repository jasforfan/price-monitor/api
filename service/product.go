package service

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	cfg "gitlab.com/jasforfan/price-monitor/api/config"
	st "gitlab.com/jasforfan/price-monitor/api/storage/product"
	pp "gitlab.com/jasforfan/price-monitor/api/storage/productprice"
	cr "gitlab.com/jasforfan/price-monitor/api/crawler"
)

type ProductService struct {
	productStorage			*st.ProductStorage
	productPriceStorage		*pp.ProductPriceStorage
}

func NewProductService(conf *cfg.Config) *ProductService {
	return &ProductService{
		productStorage: 		st.New(conf),
		productPriceStorage:  	pp.New(conf),
	}
}

func (ps *ProductService) AddProductLink(w http.ResponseWriter, r *http.Request) {
	var product st.Product

	_ = json.NewDecoder(r.Body).Decode(&product)
	
	crawler, err := cr.New(product.Link)
	if err != nil {
		return	
	}

	err = crawler.GetProduct(&product)
	if err != nil {
		return
	}

	err = ps.productStorage.Add(&product)
	if err != nil {
		log.Println(err)
		json.NewEncoder(w).Encode(st.Product{})
		return
	}

	go ps.UpdateProductPrice(&product)
	go ps.FreqUpdateProductPrice(&product)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)
	return
}

func (ps *ProductService) GetProducts(w http.ResponseWriter, r *http.Request) {
	products, err := ps.productStorage.GetAll()
	if err != nil {
		log.Fatal("Error", err)
	}
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(products)

	return
}

func (ps *ProductService) GetProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	product, err := ps.productStorage.GetById(vars["id"])
	if err != nil {
		log.Fatal("Error", err)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)

	return
}

func (ps *ProductService) GetProductPrice(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	pp, err := ps.productPriceStorage.GetByProductId(vars["id"])
	if err != nil {
		log.Fatal("Error", err)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(pp)

	return
}

func (ps *ProductService) ReloadScheduler() {
	products, err := ps.productStorage.GetAll()
	if err != nil {
		log.Fatal("Error", err)
	}

	for _, product := range products {
		go ps.UpdateProductPrice(product)
		go ps.FreqUpdateProductPrice(product)
		time.Sleep(100 * time.Millisecond)
	}
}

func (ps *ProductService) UpdateProductPrice(product *st.Product) {
	log.Println("Updating product price: " + product.Id)
	crawler, err := cr.New(product.Link)
	if err != nil {
		log.Println(err)
		return	
	}

	err = crawler.GetProduct(product)
	if err != nil {
		log.Println(err)
		return
	}

	err = ps.productStorage.Update(product)
	if err != nil {
		log.Println(err)
		return
	}

	productPrice := pp.ProductPrice{
		ProductId:	product.Id,
		Price:		product.Price,
		Currency:	product.PriceCurrency,
	}

	err = ps.productPriceStorage.Add(&productPrice)
	if err != nil {
		log.Println(err)
	}
	return
}

func (ps *ProductService) FreqUpdateProductPrice(product *st.Product) {
	for range time.NewTicker(60 * time.Minute).C {
		go func(p *st.Product) {
			ps.UpdateProductPrice(p)
		}(product)
	}
}