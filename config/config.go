package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	ServicePort			string `envconfig:"service_port" default:"8080"`
	MysqlHost			string `envconfig:"mysql_host" default:"localhost"`
	MysqlSchema			string `envconfig:"mysql_schema" default:"price_monitor"`
	MysqlUser			string `envconfig:"mysql_user" default:"user"`
	MysqlPassword		string `envconfig:"mysql_password" default:"password"`
}

func New() (conf *Config) {
	conf = new(Config)
	envconfig.Process("JFF", conf)
	return
}