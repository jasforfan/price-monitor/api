--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` char(36) COLLATE utf8_bin NOT NULL,
  `name` varchar(250) COLLATE utf8_bin NOT NULL,
  `brand` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `link` varchar(300) COLLATE utf8_bin NOT NULL,
  `price` float DEFAULT NULL,
  `price_currency` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);


--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` char(36) COLLATE utf8_bin NOT NULL,
  `product_id` char(36) COLLATE utf8_bin NOT NULL,
  `price` float NOT NULL,
  `currency` varchar(3) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`);