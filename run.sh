go get github.com/gocolly/colly \
        github.com/gorilla/mux \
        github.com/kelseyhightower/envconfig \
        github.com/satori/go.uuid \
        github.com/go-sql-driver/mysql

go build
./api
